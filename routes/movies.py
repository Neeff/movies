from flask import Blueprint, request, jsonify, make_response
from models.movie import Movie
from utils.db import db

movies = Blueprint("movies", __name__)
@movies.route('/')
def index():
    try:
        movies = Movie.query.all()
        movies = list(map(lambda movie: movie.to_json(), movies))
        response = jsonify(movies)
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    except Exception as ex:
        return jsonify({ 'message': str(ex) }, 500)

@movies.route('/new', methods=['POST'])
def add_movie():
    try:
        if request.method == 'POST':
            # create a new Movie object
            body = request.json
            new_movie = Movie(**body)

            # save the object into the database
            db.session.add(new_movie)
            db.session.commit()
            # return new movie object
            return jsonify(new_movie.to_json())
    except Exception as ex:
        return jsonify({ 'message': str(ex) })

@movies.route("/update/<string:id>", methods=["GET", "POST"])
def update(id):
    try:
        # get contact by id
        movie = Movie.query.get(id)

        if request.method == "POST" and movie is not None:
            body = request.json
            movie.name = body['name']
            movie.stars = body['stars']
            movie.released = body['released']
            movie.cover = body['cover']
            movie.category_id = body['category_id']

            db.session.commit()
            return jsonify(movie.to_json())
        if request.method == 'GET' and movie is not None:
            return jsonify(movie.to_json())
        return jsonify({ 'message': 'record not found'})
    except Exception as ex:
        return jsonify({ 'message': str(ex) })

@movies.route("/delete/<id>", methods=["DELETE"])
def delete(id):
    try:
        movie = Movie.query.get(id)
        if movie is not None:
            db.session.delete(movie)
            db.session.commit()

            return jsonify({ 'message': 'Movie deleted successfully'})
        return jsonify({ 'message': 'record not found' })
    except Exception as ex:
        return jsonify({ 'message': str(ex) })
