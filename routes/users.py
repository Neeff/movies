from flask import Blueprint, request, jsonify
from models.user import User
from utils.db import db

users = Blueprint("musers", __name__)


@users.route('/')
def index():
    try:
        users = User.query.all()
        users = list(map(lambda user: user.to_json(), users))
        return jsonify(users)
    except Exception as ex:
        return jsonify({ 'message': str(ex) }, 500)


@users.route('/new', methods=['POST'])
def add_user():
    try:
        if request.method == 'POST':
            # create a new user object
            body = request.json
            new_user = User(**body)

            # save the object into the database
            db.session.add(new_user)
            db.session.commit()
            # return new user object
            return jsonify(new_user.to_json())
    except Exception as ex:
        return jsonify({ 'message': str(ex) }, 500)


@users.route("/update/<string:id>", methods=["GET", "POST"])
def update(id):
    try:
        # get user by Id
        print(id)
        user = User.query.get(id)

        if request.method == "POST" and movie is not None:
            body = request.json
            user.first_name = body['first_name']
            user.last_name = body['last_name']
            user.email = body['email']

            db.session.commit()
            return jsonify(user.to_json())
        if request.method == 'GET' and user is not None:
            return jsonify(user.to_json())

        return jsonify({ 'message': 'record not found' })
    except Exception as ex:
        return jsonify({ 'message': str(ex) }, 500)


@users.route("/delete/<id>", methods=["DELETE"])
def delete(id):
    try:
        user = User.query.get(id)
        if user is not None:
            db.session.delete(user)
            db.session.commit()

            return jsonify({ 'message': 'User deleted successfully'})
        return jsonify({ 'message': 'record not found' })
    except Exception as ex:
        return jsonify({ 'message': str(ex) }, 500)

@users.route("/login", methods=["POST"])
def login():
    try:
        email = request.json['email']
        user = User.find_by_email(email)

        if user is not None:
            password = request.json['password']
            auth =  user.verify_password(password)
            if auth:
                return jsonify({ 'auth': auth, 'user': user.to_json() })
            return jsonify({ 'auth': auth })
        if user is None:
            return jsonify({'auth': False })
    except Exception as ex:
        return jsonify({ 'message': str(ex) })
