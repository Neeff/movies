from flask import Blueprint, request, jsonify
from models.category import Category
from models.movie import Movie
from utils.db import db

categories = Blueprint("categories", __name__)


@categories.route('/')
def index():
    try:
        categories = Category.query.all()
        categories = list(map(lambda category: category.to_json(), categories))
        return jsonify(categories)
    except Exception as ex:
        return jsonify({ 'message': str(ex) }, 500)


@categories.route('/new', methods=['POST'])
def add_category():
    try:
        if request.method == 'POST':
            # create a new category object
            body = request.json
            new_category = Category(**body)

            # save the object into the database
            db.session.add(new_category)
            db.session.commit()
            # return new movie object
            return jsonify(new_category.to_json())
    except Exception as ex:
        return jsonify({ 'message': str(ex) }, 500)


@categories.route("/update/<string:id>", methods=["GET", "POST"])
def update(id):
    try:
        # get contact by Id
        print(id)
        category = Category.query.get(id)

        if request.method == "POST" and category is not None:
            body = request.json
            category.name = body['name']

            db.session.commit()
            return jsonify(category.to_json())
        if request.method == 'GET' and category is not None:
            return jsonify(category.to_json())
        return jsonify({ 'message': 'record not found'}, 200)
    except Exception as ex:
        return jsonify({ 'message': str(ex) }, 500)


@categories.route("/delete/<id>", methods=["DELETE"])
def delete(id):
    try:
        category = Category.query.get(id)
        movies = Movie.query.filter_by(category_id=category.id).first()
        breakpoint()
        if movies is not None:
            return jsonify({ 'message': 'Error, Category associated to Movie' })
        if category is not None:
            db.session.delete(category)
            db.session.commit()

            return jsonify({ 'message': 'Category deleted successfully'})
        return jsonify({ 'message': 'record not found' })
    except Exception as ex:
        return jsonify({ 'message': str(ex) }, 500)
