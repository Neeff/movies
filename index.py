from app import app
from utils.db import db
from flask_bcrypt import Bcrypt
from flask_cors import CORS

Bcrypt(app)
CORS(app)
db.init_app(app)
with app.app_context():
    db.create_all()

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=4000)
