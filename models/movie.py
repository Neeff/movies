from utils.db import db

class Movie(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    stars = db.Column(db.Float)
    released = db.Column(db.Date)
    cover =  db.Column(db.String(500))
    category_id = db.Column(db.Integer, db.ForeignKey("category.id"))

    def __init__(self, name=None, stars=None, released=None, cover=None, category_id=None):
        self.name = name
        self.stars = stars
        self.released = released
        self.cover = cover
        self.category_id = category_id


    def to_json(self):
        return {
            'id': self.id,
            'name': self.name,
            'stars': self.stars,
            'released': self.released.strftime('%Y-%m-%d'),
            'cover': self.cover,
            'category_id': self.category_id
        }
