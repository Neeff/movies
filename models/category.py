from utils.db import db

class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    movies = db.relationship('Movie', backref='category')

    def __init__(self, name=None):
        self.name = name

    def to_json(self):
        return {
            'id': self.id,
            'name': self.name,
            'movies': list(map(lambda movie: movie.to_json(), self.movies))
        }
