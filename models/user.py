from utils.db import db
from utils.bcrypt import bcrypt



class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(30))
    last_name = db.Column(db.String(30))
    email = db.Column(db.String(30), index=True, unique=True)
    password = db.Column(db.String(128))

    def __init__(self, first_name=None, last_name=None, email=None, password=None):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.password = self.__encrypted_password(password)

    def to_json(self):
        return {
            'id': self.id,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'email': self.email
        }

    @classmethod
    def find_by_email(self, email=None):
        return User.query.filter_by(email=email).first()

    def verify_password(self, password):
        return self.__decrypt_password(password)

    def __encrypted_password(self, password):
        return bcrypt.generate_password_hash(password)

    def __decrypt_password(self, password):
        return bcrypt.check_password_hash(self.password, password)
