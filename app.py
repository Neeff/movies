from flask import Flask
from routes.movies import movies
from routes.users import users
from routes.categories import categories
from config import DevelopmentConfig
app = Flask(__name__)

# app.config.from_object(DevelopmentConfig())
app.config["SQLALCHEMY_DATABASE_URI"] = 'mysql+pymysql://root:@localhost:3306/movies_api'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['ENV'] = 'development'

def resource_not_found(error):
    return { 'message': 'resource not found', 'code': 404 }, 404

app.register_blueprint(movies, url_prefix='/api/movies')
app.register_blueprint(users, url_prefix='/api/users')
app.register_blueprint(categories, url_prefix='/api/categories')
app.register_error_handler(404, resource_not_found)
